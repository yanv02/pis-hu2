# Datei-Transfer als verteilte Anwendung

 * Transportschicht: TCP/IP-Verbindung über Sockets
 * Anwendungsschicht: Dateitransfer-Protokoll

### Phase 1. Sitzungsaufbau
 der Server kann maximal 3 client gleizeitzig bedienen
 Ein Client clickt Start. Der Server antwortet im Erfolgsfall mit ACK , im Fehlerfall mit DND .
 Dieser Fall tritt ein, wenn beim Server bereits drei Sitzungen bestehen. Der Server baut dann auch 
 die TCP/IP-Verbindung mit dem Client ab, und schließt den Kommunikationssocket.

### Phase 2. Sitzung
Ein Client kann jetzt die Dienste des Datei-Servers nutzen und Dateien transferieren, indem er 
folgende Button Clickt und der Dateienname(***hello.txt***) angibt oder auswäaht:

   | Client | Action|
   |--------|----------------|
   | *LST* | fordert eine Liste der beim Datei-Server vorhandenen Dateien an . |
   | *UPLOAD* |(PUT) übertragt eine lokale, beim Client vorhandene Datei zum Server, der die Datei bei sich ablegt |
   | *DOWNLOAD* |(GET) fordert eine Datei vom Server an, die nach Auslieferung beim Client abgelegt wird. |
   | *DEL* | an löscht eine Datei auf dem Server. |
   

### Phase 3. Sitzungsabbau
 Der Client clickt auf  END, der Server antwortet mit DSC, um den Abbau zu bestätigen.
 Die TCP/IPVerbindung    zwischen Client und Server wird abgebaut, insbesondere werden die 
 Kommunikationssockets auf beiden Seiten geschlossen. 

##Sitzungsabbaubild

![](src/pis/hue2/client/startbild.png)

