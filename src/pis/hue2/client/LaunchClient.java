package pis.hue2.client;

import java.awt.*;

public class LaunchClient  {

    public static void main(String[] args) {

        EventQueue.invokeLater(() -> {
            try {
                new ClientGUI();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });


    }
}
