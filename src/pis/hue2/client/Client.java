package pis.hue2.client;


import pis.hue2.server.Instruction;

import javax.swing.*;
import java.awt.*;
import java.net.*;
import java.io.*;
/**DEr Client fragt eine Verbindund an Server ,wenn die aczeptiert ist,dann koennen die beiden file austauchen*/
public class Client {

    /*socket : Socket
    final port : int
    final  address : String
    in : InputStream
    os :  OutputStream
    zumServer: PrintWriter
    vomServer: BufferedReader
    + {static} {final} projeckpath : String*/
    private Socket socket ; //socket : Socket
    private final int port ;// final port : int
    private final String address ; //final  address : String
    InputStream in;
    FileOutputStream frout;
    FileInputStream frin;
    OutputStream os;
    //PrintWriter pw;
    PrintWriter zumServer;
    BufferedReader vomServer;
    public static final      String projeckpath = new File("").getAbsolutePath();

    public Client( String address, int port) {
        // establish a connection
        this.address = address;
        this.port = port;
    }
    /**hier wird die Verbindung an Server gefragt*/
    public synchronized void connect() throws IOException {
        try {
            socket = new Socket(address, port);
            zumServer = new PrintWriter(
                    socket.getOutputStream(),true);
            vomServer = new BufferedReader(
                    new InputStreamReader(
                            socket.getInputStream()));
            sendNachricht(Instruction.CON.name());
        } catch (IOException e) {
            throw new IOException(e);
        }

    }
    /**der  client eine Nachricht an Servert
     */
    public synchronized void sendNachricht(String nachricht) throws IOException {
        zumServer.println(nachricht);
    }
    /**der client bekommt eine Nachricht von Server
     *@return  Nachricht*/
    public synchronized String getNachricht() throws IOException {

        String text = null; // vom Server empfangen
        try {
            text = vomServer.readLine();
        } catch (IOException e) {
            throw new IOException(e);

        }
        catch (NullPointerException e) {

                throw new IOException("der server ist geschlossen");
        }
        return text;
    }
    /**bekommmt Fileliste von Server und gibt als Array von String zurueck
     * @return msg*/
    public synchronized String[] listAusgeben() throws IOException {
        if(socket.isClosed()){
            throw new SocketException("Socket is closed");
        }
        sendNachricht(Instruction.LST.name());
        if(getNachricht().equals(Instruction.DAT.name())) {
            int lange = Integer.valueOf(getNachricht());
            String[] msg = new String[lange];
            for (int i = 0; i < lange; i++) {
                msg[i] = getNachricht();
            }
            return msg;
        }else{
            String msg[] = {"##dat error"};
            return msg;
        }
    }
    /**sagt der server der er der file filename löshen soll
     *@param fileName
     */
    public synchronized boolean deletefile(String fileName) throws IOException {
        if (fileName.equals("")){
            throw new IllegalArgumentException("File Name ist nicht gegeben");
        }else {
            sendNachricht(Instruction.DEL.name());
            String anwort2 = getNachricht();
            if (anwort2.equals("schikt der file name")) {
                sendNachricht(fileName);
            } else {
                System.out.println(" delele probleme:" + anwort2);
                sendNachricht(Instruction.DND.name());
                return false;
            }
            String antwort3 = getNachricht();
            if (antwort3.equals(Instruction.ACK.name())) {
                System.out.println("delete succes");
                return true;
            } else if (antwort3.equals("file not found")){
                throw new IllegalArgumentException("File not Fund");

            }else if (antwort3.equals(Instruction.DND.name())){
                throw new IllegalArgumentException("File not Fund");

            }

        }
        return false;
    }
    /**bekommt einen File vom server
     * @param  filename*/
    public synchronized boolean getFile(String filename) throws IOException {
        if (filename.equals("")){
            throw new IllegalArgumentException("File Name ist nicht gegeben");
        }else {
            String path1 ;
            String path2;
            path1 = ordnerauwahlen();
            if(path1.equals("")){
               path2 = projeckpath + "\\src\\pis\\hue2\\client\\Clientfile\\" + filename;
            }else path2 =path1 + "\\"+filename;
            sendNachricht(Instruction.GET.name());
            if(getNachricht().equals(Instruction.ACK.name())) {
                sendNachricht(Instruction.ACK.name());
                String anwort2 = getNachricht();
                if (anwort2.equals("schikt der file name")) {
                    sendNachricht(filename);
                } else {
                    System.out.println(" getfile filename probleme:" + anwort2);
                    sendNachricht("end");
                    //socket.close()
                    return false;
                }


                if (getNachricht().equals(Instruction.DAT.name())) {
                    sendNachricht("wie gross ist der file");
                    int grosse = Integer.valueOf(getNachricht());
                    byte[] b = new byte[(int) grosse];
                    InputStream in = (socket.getInputStream());
                    FileOutputStream fr = new FileOutputStream(path2);

                    int len;
                    if ((len = in.read(b)) != -1) {
                        fr.write(b, 0, len);
                    }
                }else{
                    throw new IllegalArgumentException("DAT error");
                }


            }else return false;
            return true;
        }

    }
    /**schickt einem Flie zu server
     * @param  filename
     */
    public synchronized boolean putFile(String filename) throws IOException {
        String path2;
        String[] daten =fileauswalen();
        path2 = daten[0];
        if(path2.equals("")){
            path2 = projeckpath +"\\src\\pis\\hue2\\client\\Clientfile\\"+ filename;
        }else filename =daten[1];
        sendNachricht(Instruction.PUT.name());
        long grosse = new File(path2).length();
        //if(getNachricht().equals(Instruction.ACK.name())) {
            String antwort2 = getNachricht();

            File f = new File(path2);
            if (f.isFile()) {
                if (antwort2.equals("schikt der file name")) {
                    sendNachricht(filename);
                } else {
                    sendNachricht("File error");
                    System.out.println(" put file probleme:" + antwort2);
                    return false;
                    //socket.close();
                }
                String antwort3 = getNachricht();
                if (antwort3.equals("was ist filelange?")) {
                    sendNachricht(String.valueOf(grosse));
                }else sendNachricht("error size");
                if(getNachricht().equals(Instruction.ACK.name())) {
                    sendNachricht(Instruction.DAT.name());
                    dat((int) grosse, f);
                }

            } else {
                sendNachricht("File error");
                throw new IllegalStateException("File is not found");
            }
            if (getNachricht().equals("ACK")) {
                System.out.println("get succes");
                return true;
            } else {
                System.out.println(" get probleme:");
                return false;
                //socket.close();
            }
        //}else return false;

    }
    public synchronized  void dat(int grosse, File f) throws IOException {
        frin = new FileInputStream(f);
        os = socket.getOutputStream();


        byte[] b = new byte[(int)grosse];
        int len;
        while ((len = frin.read(b)) != -1) {
            os.write(b, 0, len);
        }
        frin.close();
    }
    /**gibt der client socket züruckt, es wird bei gui benutzt
     * @return Socket Clientsocket*/
    public synchronized Socket getSocket(){
        return socket;

    }
    /**hier wird die verbindung mit dem Server geschlossen*/
    public boolean end () throws IOException {
        try {

                sendNachricht(Instruction.DSC.name());
                if(getNachricht().equals(Instruction.DSC.name())) {
                    getSocket().close();
                    if (getSocket().isClosed()) {
                        return true;
                        //textR.setText(Instruction.DSC.name());
                    } else {
                        return false;
                        //textR.setText("Stack is noch auf");
                    }
                }

        } catch (IOException ex) {
            throw new IOException("end" +ex);
            //JOptionPane.showMessageDialog(end,ex);
        }
        catch (NullPointerException ex) {
            throw new NullPointerException("end" +ex);
            //JOptionPane.showMessageDialog(end,ex);
        }
        /*socket.close();
        in.close();
        frin.close();
        frout.close();
        os.close();
        vomServer.close();*/
        return true;

    }
    /**hier wird ein order ausgewaht
     * @return path der path zu ordner wird als String ausgebeben*/
    private String ordnerauwahlen() {
        String path ="";
        JFileChooser fc = new JFileChooser();
        fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY );
        Component jPanel1 = null;
        int returnVal = fc.showOpenDialog(jPanel1);
        File f;
        if(returnVal == JFileChooser.APPROVE_OPTION){
            f = fc.getSelectedFile();
            path =f.getPath();
        }
        return path;
    }
    /**hier kann man einen file auswahen
     * @return []Anwort als antwort krieg man den Path(1) und der fileName(2) in einem Array von Strin*/
    public String[] fileauswalen(){
        String path ="";
        File f;
        JFileChooser fileChooser = new JFileChooser();
        int choice = fileChooser.showOpenDialog(null);
        if(choice == JFileChooser.APPROVE_OPTION) {
            f = fileChooser.getSelectedFile();
            path = f.getPath();
      /*Übergabe an Methode zum Lesen der Datei
        mit dem selektierten Dateinamen*/
        }
        //filename extrahieren
        String prufinhalt ="";
        int index = path.length()-1;
        String fui = String.valueOf("\\".charAt(0));
        String filenameungedeht ="";
        while (!prufinhalt.equals(fui)){
            prufinhalt = String.valueOf(path.charAt(index));
            filenameungedeht+=prufinhalt;
            index--;
        }
        String filename ="";
        for (int i = filenameungedeht.length()-2; i >= 0; i -- ){
            filename+= filenameungedeht.charAt(i);
        }
        System.out.println(filename);
        String[] anwort ={path,filename};

        return anwort;
    }

    public static void main(String[] args) {

        String path ="";
        File f;
        JFileChooser fileChooser = new JFileChooser();
        int choice = fileChooser.showOpenDialog(null);
        if(choice == JFileChooser.APPROVE_OPTION) {
            f = fileChooser.getSelectedFile();
            path = f.getPath();
      /*Übergabe an Methode zum Lesen der Datei
        mit dem selektierten Dateinamen*/
        }
        //System.out.println(path);


        //filename extrahieren
        String prufinhalt ="";
        int index = path.length()-1;
        String fui = String.valueOf("\\".charAt(0));
        String filenameungedeht ="";
        while (!prufinhalt.equals(fui)){
            prufinhalt = String.valueOf(path.charAt(index));
            filenameungedeht+=prufinhalt;
            index--;
        }
        String filename ="";
        for (int i = filenameungedeht.length()-2; i >= 0; i -- ){
            filename+= filenameungedeht.charAt(i);
        }
        String[] anwort ={path,filename};
        System.out.println(anwort[1]+anwort[0]);

    }

}
