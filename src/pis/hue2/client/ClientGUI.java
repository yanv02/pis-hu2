package pis.hue2.client;

import pis.hue2.server.Instruction;

import java.io.*;
import java.util.concurrent.ExecutionException;
import javax.swing.JFileChooser;
import javax.swing.*;



public  class ClientGUI extends JFrame  {

    private boolean  ergebniss ;
    final JPanel _panel;
    final JTextField textS;
    final JTextArea textR;
    final JButton list;
    final JButton get;
    final JButton put;
    final JButton del;
    final JButton start;
    final JButton end;
    private Client client;
    private String  msg ="";
    //static int anzalclient;
    //static JTextField guianzahlclient = new JTextField();
    //final JButton actuel;


    /*
    - ergebniss : boolean
    - _panel : JPanel
    - textS : JTextField
    - textR : JTextArea
    - list : JButton
    - del : JButton
    - put : JButton
    - get : JButton
    - start : JButton
    - end : JButton
    - s :Socket
    _ client : Client
    {static} msg: String
    turn : boolean

     */






    /**
     * Create the application.
     */
    public ClientGUI() {
        _panel = new JPanel();
        setVisible(true); //we make the window visible
        setResizable(false); // if write false, you won’t be able to resize the window
        setTitle("Example"); //you set the title for the window
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null); // you make the window appear at the center of the screen
        setSize(400, 500); // you set the size of the window

        this.textS = new JTextField("hallo.txt");
        this.textR = new JTextArea("ersmal auf 'start' druken\num die verbindung auf zu bauen\n" +
                "\ndefault downlood ordner ist: "+"\n"
                +"\\\\src\\\\pis\\\\hue2\\\\client\\\\Clientfile"+"\n"+
                "\nwenn du fertig bist, bitte 'end' drucken");
        this.list = new JButton("LIST");
        this.get = new JButton("DOWNLOAD");
        this.put = new JButton("UPLOAD");
        this.del = new JButton("DELETE");
        this.start = new JButton("start");
        this.end = new JButton("end");
        //this.actuel = new JButton("\uD83D\uDD01");

        add(_panel);
        _panel.setLayout(null); // you set the Layout to null = Absolute Layout

        textS.setBounds(10, 10, 120, 20);
        textS.setSize(200, 24);
        list.setBounds(220, 10, 120, 20);
        get.setBounds(220, 50, 120, 20);
        put.setBounds(220, 90, 120, 20);
        del.setBounds(220, 130, 120, 20);
        textR.setBounds(10, 50, 85, 20);
        textR.setSize(200, 250);
        start.setBounds(10, 320, 85, 20);
        end.setBounds(110, 320, 85, 20);
        //actuel.setBounds(10, 400, 50, 25);
        //guianzahlclient.setBounds(80, 400, 50, 25);


        _panel.add(textS);
        _panel.add(list);
        _panel.add(get);
        _panel.add(put);
        _panel.add(del);
        _panel.add(textR);
        _panel.add(start);
        _panel.add(end);
        //_panel.add(actuel);
        //_panel.add(guianzahlclient);

        start.addActionListener(evt -> {
            SwingWorker work = new SwingWorker() {
                @Override
                protected Void doInBackground() {
                    client = new Client("localhost", 4040);
                    try {
                        client.connect();
                        start.setEnabled(false);
                        end.setEnabled(true);

                    } catch (IOException ex) {
                        JOptionPane.showMessageDialog(start,ex);
                        start.setEnabled(true);
                        end.setEnabled(false);
                    }


                    return null;
                }
                @Override
                public void done() {
                    try {
                        msg = client.getNachricht();
                        textR.setText(msg + "\n");
                        if(msg.equals(Instruction.DND.name())){
                            textR.setText("der server hat shon 3 Client" + "\n");
                            client.getSocket().close();
                            start.setEnabled(true);
                            end.setEnabled(false);
                        }
                        msg = "";
                    } catch (IOException ex) {
                        JOptionPane.showMessageDialog(null,ex);
                    }
                }
            };
            work.execute();
        });
        end.addActionListener(e -> {
            try {
                if(client.getSocket().isConnected() && client.getSocket()!= null) {
                    boolean shloss = client.end();
                    if(shloss){
                        textR.setText(Instruction.DSC.name()+"\n"+"tchüss");
                        start.setEnabled(true);
                        end.setEnabled(false);
                    }else{
                        textR.setText("Stack is noch auf");
                        start.setEnabled(false);
                        end.setEnabled(true);
                    }
                }else {
                    start.setEnabled(true);
                    end.setEnabled(false);
                }
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(end,ex);
            }
            catch (NullPointerException ex) {
                JOptionPane.showMessageDialog(end,ex);
            }
        });
        list.addActionListener(e -> {
            SwingWorker worker = new SwingWorker() {
                @Override
                protected Object doInBackground()   {

                    try {
                        if(client.getSocket().isConnected()){
                            if(!client.listAusgeben()[0].equals("##dat error")) {
                                for (String s : client.listAusgeben()) {
                                    msg = msg + s + "\n";
                                }
                            }else msg = "dat error";
                        }else {
                            msg = "Socket is closed";
                        }
                    } catch (Exception ex ) {
                        JOptionPane.showMessageDialog(list ,"Socket is closed");
                    }

                    return null;
                }
                public void done() {
                    try {
                        get();
                        textS.setText("");
                        textR.setText(msg);
                        msg = "";
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    } catch (ExecutionException e) {
                        //e.getCause().printStackTrace();
                        new JOptionPane();
                        JOptionPane.showMessageDialog(list ,"Client is not conncted");
                    }
                }
            };
            worker.execute();
        });
        get.addActionListener(e -> {
            SwingWorker worker = new SwingWorker() {
                @Override
                protected Object doInBackground() throws Exception {
                    if(!client.getSocket().isClosed()) {
                        ergebniss = client.getFile(textS.getText());
                    }else {
                        JOptionPane.showMessageDialog(list ,"Socket is closed");
                    }
                    return null;
                }
                @Override
                protected void done() {
                    try {
                        get();

                        if(ergebniss) {
                            textR.setText("GET DONE");
                        }else {
                            textR.setText("GET ERROR");
                        }

                    }catch (ExecutionException e){
                        //e.getCause().printStackTrace();
                        String msg =String.format("Unexpected problem: %s", e.getCause().toString());
                        new JOptionPane();
                        JOptionPane.showMessageDialog(null ,msg);

                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }
            };
            worker.execute();
        });
        put.addActionListener(e -> {

            SwingWorker  worker = new SwingWorker() {
                @Override
                protected Object doInBackground() throws Exception {
                    if(!client.getSocket().isClosed()) {
                        ergebniss = client.putFile(textS.getText());
                    }else {
                        JOptionPane.showMessageDialog(put ,"Socket is closed");
                    }
                    return null;
                }
                protected  void done()  {
                    try {
                        get();
                        if(ergebniss) {
                            textR.setText("PUT IS DONE");
                        }else {
                            textR.setText("PUT ERROR");
                        }
                    }catch (ExecutionException e){
                        //e.getCause().printStackTrace();
                        String msg =String.format("Unexpected problem: %s", e.getCause().toString());
                        new JOptionPane();
                        JOptionPane.showMessageDialog(put ,msg);

                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }
            };
            worker.execute();
        });
        del.addActionListener(e -> {
            SwingWorker worker = new SwingWorker() {
                @Override
                protected Object doInBackground() throws Exception {
                    if(!client.getSocket().isClosed()) {
                        ergebniss = client.deletefile(textS.getText());
                    }else {
                        JOptionPane.showMessageDialog(del ,"Socket is closed");
                    }
                    return null;
                }

                @Override
                protected void done() {
                    try {
                        get();
                        if(ergebniss) {
                            textR.setText("DEL IS DONE");
                        }else {
                            textR.setText("DEL ERROR");
                        }
                    }catch (ExecutionException e){
                        //e.getCause().printStackTrace();
                        String msg ;
                        msg =String.format("Unexpected problem: %s", e.getCause().toString());
                        new JOptionPane();
                        JOptionPane.showMessageDialog(del ,msg);

                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }

            };
            worker.execute();
        });
        /*
        actuel.addActionListener(e -> {
            try {

                if (client.getSocket().isClosed()) {
                    guianzahlclient.setText("socket ist close");
                } else {
                    client.sendNachricht("CLI");
                    guianzahlclient.setText(client.getNachricht());
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(end,ex);
            }

        });
        */
    }



}
