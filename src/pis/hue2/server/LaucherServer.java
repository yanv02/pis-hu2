package pis.hue2.server;

import java.net.ServerSocket;
import java.net.Socket;

public class LaucherServer extends Thread {
    static boolean connect = true;

    public static void main(String[] args) {
        try {
            ServerSocket serverSocket = new ServerSocket(4040);
            while (connect){
                Socket clientSocket = serverSocket.accept();
                Server s1 = new Server(clientSocket, serverSocket);
                s1.connect();
                s1.start();

            }


        } catch (Exception ex) {
            System.out.println("try main " + ex);
        }
    }
}
