package pis.hue2.server;

import java.net.*;
import java.io.*;
import java.util.ArrayList;

/**
 * Der Server kann client( maximal 3 ) nehmmen  und file austaushen*/
public class Server  extends Thread{

    public static final      String projeckpath = new File("").getAbsolutePath(); //+ projeckpath: String
    private String pathzunutzen;
    private    final              Socket clientSocket ; //- clientSocket : Socket
    InputStreamReader vonClient; // vonClient: InputStreamReader
    BufferedReader bf; // bf: BufferedReader
    static int               anzahlvonclient ; // Anzahl der Clients insgesamt    // anzahlvonclient: int
    ServerSocket serverSocket ;

    static boolean closeConnection = false; // closeConnection: boolean
    static ArrayList<Socket> listClient = new ArrayList<Socket>();

    public Server( Socket clientSocket ,ServerSocket serverSocket) {
        this.clientSocket = clientSocket;
        this.serverSocket = serverSocket;
    }
    /**
     * Hier wird ein Nachricht an Client geshickt ob die Verbindung Ackzeptiert oder abgeleht wurde
     */
    public synchronized Socket connect() throws IOException {
        try {
            String anwort = getNachricht();
            System.out.println("intstruc ="+anwort);
            if(anwort.equals(Instruction.CON.name())) {
                if (listClient.size() < 3) {
                    sendNachricht(Instruction.ACK.name());
                    anzahlvonclient++;
                    listClient.add(clientSocket);
                    closeConnection = false;
                } else sendNachricht(Instruction.DND.name());
                //clientSocket.close();
            }else sendNachricht(Instruction.DND.name());

        }catch (IOException e) {
            throw  new IOException("IO-Error bei Client " + anzahlvonclient + e);
        }
        return clientSocket;

    }
    /**
     * Hier wird die Verbindung zwishen der Server und ein Client geschlossen
     * der Server wird geschlossen wenn es keine Client mehr gibt*/
    public synchronized void  end() throws IOException {

        try {
            sendNachricht("DSC");
            anzahlvonclient--;
            listClient.remove(clientSocket);
            clientSocket.close();
            //serverSchliessen();
        } catch (Exception ex) {
                throw new IOException(ex);
        }


    }
    /**der server wird geschlossen wenn er keine client währent 20 sek hat */
    public synchronized void serverSchliessen() throws IOException {

        try {
            Thread.sleep(10000);//20 sekunden
            if(listClient.size() <1){
                LaucherServer.connect = false;
                listClient.clear();
                serverSocket.close();
            }
        } catch (Exception ex) {
            throw new IOException(ex);
        }


    }
    /**der Server bekommt der Nachricht von Client
     * @return  Nachricht*/
    public synchronized String getNachricht() throws IOException {

        vonClient = new InputStreamReader(clientSocket.getInputStream());
        bf = new BufferedReader(vonClient);
        return bf.readLine();
    }

    /**Schickt eine Nachricht an Client
     * @param  info*/
    public synchronized void sendNachricht(String info) throws IOException {

        PrintWriter zumClient = new PrintWriter(
                clientSocket.getOutputStream(),true);
        zumClient.println(info);
    }
    /**die liste von ServerFlie
     * *@return Listfile
     */
    public synchronized static String[] listFile() {
        //String path =serverPath + fileName;
        String path = projeckpath +"\\src\\pis\\hue2\\server\\Serverfile";
        File file = new File(path);
        return file.list();
    }
    /**schickt einen Flie zu Client*/
    public synchronized  void getFile() {
        System.out.println("GET");

        OutputStream os;
        try {
            long grosse = new File(pathzunutzen).length();
            sendNachricht(Instruction.DAT.name());
            byte []bytesArray = new byte[(int)grosse];
            dat(grosse,bytesArray);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        }

    }
    public synchronized  void dat(long grosse, byte []bytesArray){
        OutputStream os;
        try {
            String anwort3 = getNachricht();
            if (anwort3.equals("wie gross ist der file")) {
                System.out.println(grosse);
                sendNachricht(String.valueOf(grosse));
            } else {
                System.out.println(" getfile grosse probleme:" + anwort3);
                sendNachricht("end");
                //socket.close()
            }
            FileInputStream fr = new FileInputStream(pathzunutzen);
            int len;
            os = clientSocket.getOutputStream();
            if ( (len = fr.read(bytesArray)) != -1) {
                os.write(bytesArray, 0, len);
            }
            fr.close();


        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    /**bekommt einen File vom Client
     * @param  filename*/
    public synchronized  void pufFile(String filename,int lange)  throws IOException {
        System.out.println("put it");
        if(getNachricht().equals(Instruction.DAT.name())){
            byte[] b = new byte[lange];
            InputStream in = (clientSocket.getInputStream());
            OutputStream fr = new FileOutputStream(projeckpath + "\\src\\pis\\hue2\\server" + "\\Serverfile\\" + filename);

            int len;
            if ((len = in.read(b)) != -1) {
                fr.write(b, 0, len);
            }
            fr.close();
        }else{
            throw new IllegalArgumentException("DAT error");
        }


    }

    /**hier wird der Theard mit ein switch case statement ausgefüht*/
    public  void run(){
        try {
            String bephel;

            while (closeConnection == false) {

                bephel = getNachricht();
                System.out.println(bephel);
                /*hier werden die instruction von Server behandelt*/
                switch (bephel) {
                    case "CLI" -> {
                        sendNachricht(String.valueOf(listClient.size()));
                        
                    } //instruction
                    case "DEL" -> {
                        System.out.println("delete case");
                        sendNachricht("schikt der file name");
                        String fileName = getNachricht();
                        if(!fileName.equals(Instruction.DND.name())) {
                            System.out.println(fileName);
                            String path = projeckpath + "\\src\\pis\\hue2\\server\\Serverfile\\" + fileName;
                            File file = new File(path);
                            if (file.isFile() && file.exists()) {
                                boolean succes =  file.delete();
                                if(succes){sendNachricht(Instruction.ACK.name());
                                }else sendNachricht(Instruction.DND.name());

                            } else {
                                sendNachricht("file not found");
                            }
                        }


                    }
                    case "DSC" -> {
                        closeConnection = true;
                        end();
                    }
                    case "GET" ->{
                        System.out.println("get");
                        sendNachricht(Instruction.ACK.name());
                        if(getNachricht().equals(Instruction.ACK.name())) {
                            sendNachricht("schikt der file name");
                            String fileName = getNachricht();
                            if (fileName.equals("end")) {

                            } else {
                                System.out.println(fileName);
                                pathzunutzen = projeckpath + "\\src\\pis\\hue2\\server\\Serverfile\\" + fileName;
                                File f = new File(pathzunutzen);
                                if(f.exists()) {
                                    getFile();
                                }else {sendNachricht("not found");}
                            }
                        }

                    }
                    case "LST" -> {
                        int lange = listFile().length;
                        sendNachricht(Instruction.DAT.name());
                        sendNachricht(String.valueOf(lange));
                        for (String i : listFile()) {
                            sendNachricht(i);
                        }

                    }
                    case "PUT" -> {
                        System.out.println("put");
                        sendNachricht("schikt der file name");
                        String fileName = getNachricht();
                        if (fileName.equals("File error")){
                            break;
                        }else {
                            sendNachricht("was ist filelange?");
                            int fileLange = Integer.valueOf(getNachricht());
                            System.out.println("filename ="+fileName);
                            System.out.println("langue = "+fileLange);
                            if(fileName != null && fileLange!=0) {
                                sendNachricht(Instruction.ACK.name());
                                pufFile(fileName, fileLange);
                                sendNachricht("ACK");
                            }else sendNachricht("DND");
                        }

                    }
                    case "CON" -> {
                        //connect();
                    }
                    default -> throw new IllegalStateException("Unexpected value bei bephel: " + bephel);
                }
            }
        } catch (SocketException e){
            listClient.remove(clientSocket);
            throw new IllegalStateException("Connection reset : der Client ist weg");
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }


}
